///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file the.cpp
/// @version 2.0
///
/// Unit test for this lab
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   20_04_2021
//////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cassert>

#include "cat.hpp"

using namespace std;

int main(){
   cout << "Hello World" << endl;
 
   CatEmpire cat;

   cout << "Is it empty: " << boolalpha << cat.empty() << endl;
   
   cout << "Adding a cat" << endl;
   cat.addCat(new Cat("tom"));
   cout << "Is it empty: " << boolalpha << cat.empty() << endl;
   
   cout << "Testing getEnglishSuffix, should print up to 30th Generation" << endl;
   for(int i = 1; i < 31; i++){
      cat.getEnglishSuffix(i);
      cout << " Generation" << endl;
   }

   return 0;

}
